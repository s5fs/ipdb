README.md

IPDB is an overcomplicated way of viewing where failed ssh logins originated.

### Current design

__Frontend__
1. Simple web frontend makes an AJAX request to a serverless-deployed lambda function.
2. Lambda runs a scan for all the records in the DynamoDB "ipdb" table
3. That's it.

__Backend__
1. Agent is installed and listens for new entries into /etc/hosts.deny.
2. When a new log entry is created, we publish the IP address to an SNS topic.
3. Lambda function listens for the record from the SNS message and looks up the IP in DynamoDB to ensure it's new.
4. If it's new, use a geolookup service to map the IP back to lat/long.
5. Inserts the completed record into DynamoDB for safekeeping.
6. ~fin~

### Build and deploy

_Prereqs_
1. AWS CLI configured and ready to go
2. Recent Node.js, recent Serverless framework

_Usage_
1. Run 'npm run deploy' to deploy the whole solution stack

### TODO

1. Include Route53 in serverless
2. Create Ansible playbook to install the agent
3. Build an Alexa Skill for getting IPDB stats
    * "Alexa, ask IPDB for the last|latest|newest record"
    * "Alexa, ask IPDB for the last|latest|newest 5 records"
    * "Alexa, ask IPDB for the record count"
    * Future
    * "Alexa, how many IPDB entries from {country}?"
    * "Alexa, how many IPDB entries in the last month?"
    * "Alexa, how many IPDB entries "

