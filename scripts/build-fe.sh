#!/bin/bash

URL=$1

cd src/map

# Create artifact directory
# Process html "template"
if [ -d "tmp" ]; then
  rm -rf tmp
fi

mkdir tmp
cp *.html tmp
cp index.tmpl tmp/index.html
sed -i -- 's,{{URL}},'${URL}',g' 'tmp/index.html'
rm 'tmp/index.html--'
