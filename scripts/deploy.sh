# Deploy the serverless application
./scripts/deploy-serverless.sh

# Move into the serverless directory and run the CLI to get the service endpoint URL
cd src/serverless
ENDPOINT="$(sls info --verbose | grep ServiceEndpoint | cut -d ' ' -f 2)"
cd ../..

echo "Endpoint is " ${ENDPOINT}

# Kick off the front-end build and publish, pass in the endpoint URL
./scripts/deploy-fe.sh ${ENDPOINT}
