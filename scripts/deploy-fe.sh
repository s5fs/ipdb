#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "tmp" ]; then
  . ${DIR}/build-fe.sh
fi

for file in `ls tmp/*.html`; do
 aws s3 cp ${file} s3://ipdb-dev-fe --acl public-read
done
