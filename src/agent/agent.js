/* eslint no-console: 0 */
const AWS = require('aws-sdk')
AWS.config.update({region: 'us-west-2'})
const config = require('./config.json')
const os = require('os')
const Tail = require('always-tail')
const tail = new Tail(config.file, '\n')
const sns = new AWS.SNS({apiVersion: '2010-03-31'})
const log = function (message, data) {
  if (typeof data === 'string') {
    message += data
  }
  console.log(`${appname}: ${message}`)
}

const appname = config.appname
const hostname=os.hostname()

log('agent started')

tail.on('line', data => {
  log('Received: ', data)
  save(data)
})

tail.on('error', error => {
  log('Error: ', error)
})

function save (data) {
  if (!data || typeof data !== 'string') {
    return log('No data, cannot save')
  }

  const params = {
    MessageAttributes: {
      'agent': {
        DataType: 'String',
        StringValue: appname
       },
      'host': {
        DataType: 'String',
        StringValue: hostname
      }
    },
    Message: JSON.stringify({
      ip: data.replace('sshd: ', '')
    }),
    TopicArn: config.sns_topic
  }

  sns.publish(params, function(err, data) {
    if (err) {
      log('Error: ', err)
    } else {
      log('Sent: ', data.MessageId)
    }
  })
}
