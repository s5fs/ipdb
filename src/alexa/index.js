9/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * This sample demonstrates a simple skill built with the Amazon Alexa Skills
 * nodejs skill development kit.
 * The Intent Schema, Custom Slots and Sample Utterances for this skill, as well
 * as testing instructions are located at https://github.com/alexa/skill-sample-nodejs-fact
 **/

'use strict';

const Alexa = require('alexa-sdk');

const APP_ID = "amzn1.ask.skill.932973cb-921a-42cc-afd8-e4364915781d";

const languageStrings = {
    'en': {
        translation: {
            SKILL_NAME: 'IPDB',
            HELP_MESSAGE: 'You can ask me for the total records, the newest record, or the last however many records ... What can I help you with?',
            HELP_REPROMPT: 'What can I help you with?',
            STOP_MESSAGE: 'Goodbye!',
        },
    }
};

const handlers = {
    'LaunchRequest': function () {
        // Create speech output
        const speechOutput = 'Welcome to IPDB!';
        this.emit(':tell', speechOutput);
        this.emit('AMAZON.HelpIntent');
    },
    'GetRecordCount': function () {
      const count = 'Right now there are 12,785 total records';
      this.emit(':tell', count);
    },
    'GetLastRecord': function () {
      const lastRecord = 'The last record was created 2 hours ago from an IP address in Sao Paulo, Braziil';
      this.emit(':tell', lastRecord);
    },
    'GetLastRecords': function () {
      const lastRecords = 'The last five records are none of your business';
      this.emit(':tell', lastRecords);
    },
    'WhatAboutJustin': function () {
      const sick = Math.floor(Math.random() * 5)
      const burn = [
        '<audio src="https://pokemedia.com/birdup.mp3" />bird up',
        '<p>I want to tell you a secret.</p><audio src="https://pokemedia.com/fart2.mp3" /><prosody rate="slow"><prosody pitch="high">That</prosody> guy <prosody pitch="medium">frickin<prosody volume="loud"><prosody rate="x-slow">sucks</prosody></prosody></prosody></prosody>',
        'Whoa, this is the ugliest hat I\'ve ever seen! Do you get a free bowl of soup with that hat? Oh, it looks good on you, though',
        'Frickin guy moved to capital city',
        'Are you saying that I put an abnormal brain into a seven and a half foot long, fifty-four inch wide GORILLA?'
      ]
      this.emit(':tell', burn[sick]);
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = this.t('HELP_MESSAGE');
        const reprompt = this.t('HELP_MESSAGE');
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', this.t('STOP_MESSAGE'));
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', this.t('STOP_MESSAGE'));
    },
};

exports.handler = function (event, context) {
    const alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.resources = languageStrings;
    alexa.registerHandlers(handlers);
    alexa.execute();
};
