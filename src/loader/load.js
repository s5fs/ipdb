/* eslint no-console: 0 */
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const db = new AWS.DynamoDB.DocumentClient();
const mysql = require('mysql');
const moment = require('moment');
const config = {
  geolookup: false,
  tableName: 'ipdb-dev',
  updateDb: true,
  verbose: true,
  batchSize: 25,
  queryLimit: 3000
}
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'ipdb_user',
  password : 'ipdb_user',
  database : 'ipdb'
});

function load() {
  connection.connect();

  let statement = 'SELECT ip, GeoLat AS latitude, GeoLong AS longitude, City AS city, Region_Name AS region, Country AS country, created FROM HostEntry ORDER BY created asc';
  if (config.queryLimit) {
    statement += ` LIMIT ${config.queryLimit}`;
  }
  connection.query(statement, function (error, results) {
    if (error) throw error;

    batchify(results.map(item => {
      return { PutRequest: { Item: {
        ip: item.ip,
        latitude: item.latitude,
        longitude: item.longitude,
        city: item.city,
        region: item.region,
        country: item.country,
        created: moment(item.created).valueOf()
      }}}
    }));
  });

  connection.end();
}

// Format the batches
let batches = [];

function batchify(records) {
  console.log('RECORDS LEN', records.length);
  let batch = []

  records.map((item) => {
    batch.push(item)
    if (batch.length === config.batchSize) {
      batches.push(batch)
      batch = []
    }
  })

  if (batch.length) {
    batches.push(batch)
  }

  // Save the batches
  save(0)
}

function save(index) {
  const items = batches[index];
  const params = { RequestItems: {} }
  params.RequestItems[config.tableName] = items

  if (config.updateDb) {
    console.log('Processing batch', index);
    db.batchWrite(params, (err) => {
      if (err) {
        console.log('Error', err)
      } else {
        console.log('Processed batch', index);
        index += 1;

        if (batches[index]) {
          setTimeout(() => {
            save(index);
          }, 500);
        }
      }
    })
  }
}

// Do work
load();
