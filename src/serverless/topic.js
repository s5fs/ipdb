/* eslint no-console: 0 */
const request = require('request');
const moment = require('moment');
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-west-2'});
const db = new AWS.DynamoDB.DocumentClient();
const config = {
  geolookup: true,
  tableName: 'ipdb-dev'
}

function geolookup(ip, callback) {
  if (!config.geolookup) {
    const msg = `Geolookup skipped for ${ip}`
    console.log(msg)
    return callback(msg)
  }

  const lookup = `http://freegeoip.net/json/${ip}`;
  request(lookup, (error, response, body) => {
    console.log('Geo data', body);
    let data = null;
    try {
      data = JSON.parse(body)
    } catch (ex) {
      console.log('Parse error', ex)
      return callback(ex)
    }
    return callback(error, data);
  });
}

function find (ip, callback) {
  const params = {
    TableName: config.tableName,
    Key: { ip: ip }
  };
  db.get(params, callback);
}

function save (geo, callback) {
  const fields = {
    ip: "ip",
    country: "country_name",
    region: "region_name",
    city: "city",
    latitude: "latitude",
    longitude: "longitude"
  }

  const item = {}
  Object.keys(fields).map(field => {
    if (geo[fields[field]]) {
      item[field] = geo[fields[field]]
    }
  })
  item.created = moment().unix()

  const params = {
    TableName: config.tableName,
    Item: item
  }

  db.put(params, callback)
}

module.exports.handler = (event, context, callback) => {
    const message = JSON.parse(event.Records[0].Sns.Message);
    const ip = message.ip;

    console.log('Searching for', ip);

    find(ip, (error, found) => {
      if (error) {
        return console.log('Error during find', error)
      }

      if (found.ip) {
        return console.log('Record found, no lookup needed', found.ip, found.created)
      }

      console.log('Performing geolookup for', ip)

      geolookup(ip, (error, record) => {
        if (error) {
          return console.log('Error performing geolookup!', error)
        }

        return save(record, callback)
      })
    })
};
