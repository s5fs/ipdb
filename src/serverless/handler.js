/* eslint no-console: false */
'use strict';

const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const params = {
    TableName: 'ipdb-dev',
    ProjectionExpression: 'ip, latitude, longitude, city, region_name, country, created'
};

module.exports.ipdb = (event, context, callback) => {
  const response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
    },
    body: null,
  };

  db.scan(params, onScan);

  function onScan(err, data) {
    if (err) {
      console.error('Unable to scan the table. Error JSON:', JSON.stringify(err, null, 2));
      response.statusCode = 500
    } else {
      console.log('Scan succeeded! ' + data.Items.length);
      response.body = JSON.stringify(data.Items);
    }

    return callback(null, response);
  }
};
